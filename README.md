## Тестирование в Go (источники)

- Package testing (https://golang.org/pkg/testing/)
- Практика написания модульных тестов в языке Go (https://eax.me/golang-unit-testing/)
- Package require v1.6.1 (
https://pkg.go.dev/github.com/stretchr/testify/require?tab=overview
https://pkg.go.dev/github.com/stretchr/testify/require?tab=overview
 https://pkg.go.dev/github.com/stretchr/testify/require#pkg-overview
 )
- Go: тестирование кода (https://morphs.ru/posts/2017/04/08/go-testing)
- 5 продвинутых техник тестирования на Go (https://zen.yandex.ru/media/id/5bbcbc1ba5bd5400a990e7d9/5-prodvinutyh-tehnik-testirovaniia-na-go-5ce3fc795fc16a00b0f12fa4)
- Мой подход к тестированию. Часть первая (https://ivahaev.ru/how-to-test-go-applications-my-way-part-one/)
- Мой подход к тестированию. Часть вторая (https://ivahaev.ru/how-to-test-go-applications-my-way-part-two/)
